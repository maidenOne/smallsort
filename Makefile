all:
	ctags -R 
	gcc -c main.c 
	gcc -c config.c
	gcc -o smallsort main.o config.o

clean:
	rm -rf *.o
	rm smallsort

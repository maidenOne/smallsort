#include <stdio.h>

typedef struct items_
{
	int id;
	char desic[120];
}Item;

typedef struct pos_
{
	int x;
	int y;
	int z;
}Pos;

Item storage[3][3][3];

const int width = 3;
const int height = 3;
const int depth = 3;

Pos add(Item item)
{
	for(int i = 0; i< width; i++)
	{
		for(int j=0; j<height; j++)
		{
			for(int k=0; k<depth; k++)
			{
				if(storage[i][j][k].id == 0)
				{
					storage[i][j][k] = item;
					Pos pos = {i,j,k};
					return pos;
				}
			}
		}
	}
	return (Pos){0,0,0};
}


int stackSize(int x, int y)
{
	int count = 0;
	for(int i=0; i<depth; i++)
	{
		if(storage[x][y][i].id == 0)
			count = 0;
	}
	return count;
}

int appendToStack(int x, int y, Item item)
{
	int numItems = stackSize(x,y);
	if(numItems < depth)
	{
		for(int z=numItems; z>0; z--)
		{
			storage[x][y][z+1] = storage[x][y][z];
		}
		storage[x][y][0] = item;
	}
	return 0;
}

int stackContainsId(int x, int y, int id)
{
	for(int z=0; z<depth; z++)
	{
		if(storage[x][y][z].id == id)
			return z;
	}
	return -1;
}

Item fetchItem(int id)
{
	Item item;
	item.id = 0;
	for(int x=0; x<width; x++)
	{
		for(int y=0; y<height; y++)
		{

			for(int z=0; z<depth; z++)
			{
				if(storage[x][y][z].id == id)
				{
					item = storage[x][y][z];
					storage[x][y][z].id = 0;	
					return item;
				}
			}
		}
	}
	return item;
}

int test()
{
	return 0;
}

void printStorage()
{
	for(int i = 0; i< depth; i++)
	{
		printf("Layer %d:\n", i);
		for(int j=0; j<width; j++)
		{
			for(int k=0; k<height; k++)
			{
				printf(" % 2d",storage[j][k][i].id);
			}
			printf("\n");
		}
		printf("\n");
	}
	printf("\n");

}

int main()
{
	printf("Welcome to smallSort 0.1\n\n");
	printStorage();
	for(int i=0; i<9*3; i++)
	{
		Item item = {i+1,"test"};
		Pos pos = add(item);
		printf("%d,%d,%d\n",pos.x,pos.y,pos.z);
	}
	printStorage();

	fetchItem(11);
	fetchItem(15);
	fetchItem(16);
	printStorage();
	test();
}
